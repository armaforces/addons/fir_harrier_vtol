@echo off
call tools\build\fnc.bat :argparse %1 %2 %3 %4 %5 %6 %7

:: Possible arguments:
::
:: "-sign"      Require sign, script will fail if private key is not present
:: "-nosign"    Do not sign
:: "-ds"        Use DSSign instead of armake2 for signing
:: "-publish"    Update addon on steam workshop
::
:: Mod settings
set private_key=armaforces_fir_harrier_fix.biprivatekey
set public_key=armaforces_fir_harrier_fix.bikey
set mod_base=armaforces_fir_harrier_fix
set workshop_id=1775621183

:: Build folders
call tools\build\fnc.bat :build_folder addons

exit /B %errorlevel%
