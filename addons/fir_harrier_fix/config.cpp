class CfgPatches
{
    class FIR_AV8B_F_ArmaForces
    {
        units[] = {
            "FIR_AV8B",
            "FIR_AV8B_VMA231_02",
            "FIR_AV8B_VMA211_OLD",
            "FIR_AV8B_VMA223",
            "FIR_AV8B_VMA223_OLD",
            "FIR_AV8B_NA",
            "FIR_AV8B_NA_VMA211",
            "FIR_AV8B_GR7A_Lucy",
            "FIR_AV8B_GR9A",
            "FIR_AV8B_GR9A_2"
        };
        weapons[] = {};
        requiredVersion = 1;
        requiredAddons[] = {
            "FIR_AV8B_F",
            "FIR_AirWeaponSystem_US"
        };
        author = "ArmaForces";
        authors[] = {"3Mydlo3", "Madin"};
    };
};
class CfgVehicles
{
    class thingX;
    class Motorcycle;
    class Air;
    class Plane: Air {
        class NewTurret;
        class ViewPilot;
    };
    class Plane_Base_F: Plane {
        class AnimationSources;
        class HitPoints {
            class HitHull;
        };
        class Components;
    };
    class Plane_Fighter_03_base_F: Plane_Base_F {};
    class FIR_AV8B_Base: Plane_Fighter_03_base_F {
        vtol = 1;
    };
        class FIR_AV8B_NA_Base: Plane_Fighter_03_base_F {
        vtol = 1;
    };
    class FIR_AV8B_GR7_Base: Plane_Fighter_03_base_F {
        vtol = 1;
    };
};